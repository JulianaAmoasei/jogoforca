// var botao = document.getElementById("btnCadastrar");
// botao.onclick = function(){
//     event.preventDefault();
//     alert("mensagem marota");
// };
var comecou = false;
var letrasChutadas = [];
var palavra = "";
var erro = 0;

$("#btnCadastrar").click(function (event) {
    var letrasChutadas = [];
    var erro = 0;
    event.preventDefault();
    //declarar variavel palavra com o valor do input
    palavra = $("#palavra").val();
    //verificar se o campo esta vazio
    if (palavra === "") {
        //se estiver vazio mostrar msg de erro
        alert("preencha o campo");
    } else {
        $("#boneco").length = 0;
        //se nao estiver vazio montar os underlines
        palavra = palavra.trim();
        for (i = 0; i < palavra.length; i++) {
            var span = $("<span>" + palavra[i] + "</span>");
            span.appendTo(".letras");
        }
        //se nao estiver vazio mostrar a tela forca
        // adicionar classe na tela da forca
        $("#forca").addClass("visivel");
        //remover a classe na tela do cadastro
        $("#cadastro").removeClass("visivel");
        comecou = true;
    }
});

$(document).keydown(function (event) {
    if (comecou) {
        var letra = event.key;
        // validando se é letra ou tecla de funcao
        if (letra.length > 1) {
            return;
        }
        // verificar se a letra já foi utilizada
        if (letrasChutadas.indexOf(letra) !== -1) {
            return;
        }
        // registrar a letra utilizada
        letrasChutadas.push(letra);
        var span = $("<span>" + letra + "</span>");
        span.appendTo(".letras-usadas");
        // letra existe na palavra cadastrada?
        if (palavra.indexOf(letra) != -1) {
            //se sim
            // mostra no campo a letra correspondente
            for (var i = 0; i < palavra.length; i++) {
                var letra2 = palavra[i];
                if (letra == letra2) {
                    // i é o índice que temos que mostrar na tela
                    $(".letras span").eq(i).addClass("visivel");
                }
            }
            //se a palavra estiver completa
            if ($(".letras span:not(.visivel)").length == 0) {
                //mostra final correto
                $("#ganhou").addClass("visivel");
                $("#forca").removeClass("visivel");
            }
            // se não 
        } else {
            console.log("boneco");
            //mostra o membro do boneco
            //se excedeu tentativas
            if (erro < 5) {
                $(".corpo .st0").eq(erro).attr("class", "st0 visivel");
                erro++;
                //mostra família triste
            } else {
                $("#perdeu").addClass("visivel");
                $("#forca").removeClass("visivel");
            }
        }
    }
});

//sucesso - recomecar

$("#ganhou .btn-recomecar").click(function (event) {
    event.preventDefault();
    $("#cadastro").addClass("visivel");
    $("#ganhou").removeClass("visivel");    
    $(".letras").html(null);
    $(".letras-usadas").html(null);
    palavra.length = 0;
    letrasChutadas.length = 0;
    erro= 0;
    $("#palavra").val("");
    $(".corpo .st0").attr("class", "st0");
    comecou = false;
});

$("#perdeu .btn-recomecar").click(function (event) {
    event.preventDefault();
    $("#cadastro").addClass("visivel");
    $("#perdeu").removeClass("visivel");
    $(".letras").html(null);
    $(".letras-usadas").html(null);
    palavra.length = 0;
    letrasChutadas.length = 0;
    erro = 0;
    $("#palavra").val("");
    $(".corpo .st0").attr("class", "st0");
    comecou = false;
});